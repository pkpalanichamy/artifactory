variable "enabled_metrics" {
    default = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
]
    type = "list"
}
locals {
   common_tags = {
    owner       = "{{ owner }}"
    environment = "{{ env_name }}"
    costcenter  = "{{ costcenter }}"
   }
 }

