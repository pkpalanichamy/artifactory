terraform {
  backend "s3" {
    region  = "{{ region }}"
    bucket  = "{{ state_bucketname }}"
    key     = "{{ stack_name }}_{{ role_name }}.tfstate"
  }
}

provider "aws" {
  region = "{{ region }}"
}
