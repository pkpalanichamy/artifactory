
###########################################################################################
# IAM PERMISSIONS
###########################################################################################

#define iam permissions for host
data "aws_iam_policy_document" "{{ env_name }}_{{ stack_name }}_instance_policy" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

#provides an IAM role for ec2 access
resource "aws_iam_role" "{{ env_name }}_{{ stack_name }}_ec2_instance_role" {
  name               = "{{ env_name }}_{{ stack_name }}_ec2_instance_profile"
  assume_role_policy = "${data.aws_iam_policy_document.{{ env_name }}_{{ stack_name }}_instance_policy.json}"
}

#attaches a Managed IAM Policy to an IAM ec2 role
resource "aws_iam_role_policy_attachment" "{{ env_name }}_{{ stack_name }}_instance_policy_attachment" {
  role       = "${aws_iam_role.{{ env_name }}_{{ stack_name }}_ec2_instance_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

#provides an IAM instance profile.
resource "aws_iam_instance_profile" "{{ env_name }}_{{ stack_name }}_instance_profile" {
  name = "${aws_iam_role.{{ env_name }}_{{ stack_name }}_ec2_instance_role.name}"
  role = "${aws_iam_role.{{ env_name }}_{{ stack_name }}_ec2_instance_role.name}"
}

###########################################################################################
# EC2 ACCESS
###########################################################################################

#provides a security group resource
resource "aws_security_group" "{{ env_name }}_{{ stack_name }}_instance_securitygroup" {
  vpc_id = "{{ vpc['vpc-id'] }}"
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  },
    ingress {
    from_port = 8080
    to_port = 8080
    protocol = "{{ alb_ingress_protocol | default('tcp')}}"
    cidr_blocks = ["{{ alb_ingress_cidrs | default('0.0.0.0/0')}}"]
	  description = "Artifactory Port"
  },
    ingress {
    from_port = 22
    to_port = 22
    protocol = "{{ alb_ingress_protocol | default('tcp')}}"
    cidr_blocks = ["{{ alb_ingress_cidrs | default('172.29.187.0/24')}}"]
	  description = "SSH access from the Steppingstone"
  },
  ingress {
    from_port = "{{ instance_port }}"
    to_port = "{{ instance_port }}"
    protocol = "{{ alb_ingress_protocol | default('tcp')}}"
    cidr_blocks = ["{{ alb_ingress_cidrs | default('172.29.187.0/24')}}"]
  }
  tags = "${merge(
      local.common_tags,
      map(
      "Name", "{{ env_name }}_{{ stack_name }}_instance_securitygroup"
    ))}"
}

###########################################################################################
# AUTOSCALING
###########################################################################################

#filter Name for corresponding subnet name
data "aws_subnet_ids" "{{ subnet_type }}" {
 vpc_id = "{{ vpc['vpc-id'] }}"
  tags {
    Name = "{{ env_name }}_subnet_{{ subnet_type }}*"
  }
}

 resource "aws_launch_configuration" "{{ env_name }}_{{ stack_name }}_launch_configuration" {
   image_id = "{{ ami_id }}"
   instance_type = "{{ instance_type }}"
   key_name = "{{ instance_key |default('') }}"

   lifecycle {
     create_before_destroy = true
   }
   root_block_device {
     volume_type = "{{ root_volume_type | default('gp2') }}"
     volume_size = "{{ root_volume_size | default(30) }}"
   }

   ebs_block_device = {
     device_name = "xvdb"
     volume_type = "{{ ebs_volume_type | default('gp2') }}"
     volume_size = "{{ ebs_volume_size | default(5) }}"
     encrypted = true
   }

   name_prefix = "lc_{{ env_name }}_{{ stack_name }}_ec2_instance"
   iam_instance_profile = "${aws_iam_instance_profile.{{ env_name }}_{{ stack_name }}_instance_profile.name}"


   ###########################################################################################
   # User Data Script
   ###########################################################################################


   #Provide user data for executing while launching the ec2 instance
   user_data = "${file("user_data.sh")}"

   #Query for security groups
   security_groups = [
     "${aws_security_group.{{ env_name }}_{{ stack_name }}_instance_securitygroup.id}"]

 }

resource "aws_autoscaling_group" "{{ env_name }}_{{ stack_name }}_autoscaling_group" {
  lifecycle {
    create_before_destroy = true
  }

  name                      = "asg_{{ env_name }}_{{ stack_name }}_ec2_instance"
  launch_configuration      = "${aws_launch_configuration.{{ env_name }}_{{ stack_name }}_launch_configuration.name}"
  health_check_grace_period = "{{ health_check_grace_period |default(600) }}"
  health_check_type         = "EC2"
  desired_capacity          = "{{ desired_capacity | default(1) }}"
  termination_policies      = ["OldestLaunchConfiguration", "Default"]
  min_size                  = "{{ min_size | default(1) }}"
  max_size                  = "{{ max_size | default(4)}}"
  enabled_metrics           = ["${var.enabled_metrics}"]
  vpc_zone_identifier       = ["${data.aws_subnet_ids.{{ subnet_type }}.ids}"]

  tag {
    key                 = "Name"
    value               = "{{ env_name }}_{{ stack_name }}_ec2_instance"
    propagate_at_launch = true
  }

  tag {
    key                 = "Project"
    value               = "{{ project_name | default('') }}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = "{{ env_name }}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Subnet"
    value               = "{{ subnet_type }}"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "{{ env_name }}_{{ stack_name }}_instance_scale_up" {
  name                   = "asgScalingPolicy_{{ env_name }}_{{ stack_name }}EC2ScaleUp"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = "{{ scale_up_cooldown_seconds | default(300) }}"
  autoscaling_group_name = "${aws_autoscaling_group.{{ env_name }}_{{ stack_name }}_autoscaling_group.name}"
}

resource "aws_autoscaling_policy" "{{ env_name }}_{{ stack_name }}_instance_scale_down" {
  name                   = "asgScalingPolicy_{{ env_name }}_{{ stack_name }}EC2ScaleDown"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = "{{ scale_down_cooldown_seconds | default(300) }}"
  autoscaling_group_name = "${aws_autoscaling_group.{{ env_name }}_{{ stack_name }}_autoscaling_group.name}"
}


###########################################################################################
# CLOUDWATCH RESOURCES
###########################################################################################

resource "aws_cloudwatch_metric_alarm" "{{ env_name }}_{{ stack_name }}_instance_high_cpu" {
  alarm_name          = "alarm_{{ env_name }}_{{ stack_name }}EC2CPUUtilizationHigh"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "{{ high_cpu_evaluation_periods| default(2) }}"
  metric_name         = "CPUReservation"
  namespace           = "AWS/EC2"
  period              = "{{ high_cpu_period_seconds |default(300)}}"
  statistic           = "Maximum"
  threshold           = "{{ high_cpu_threshold_percent |default(90) }}"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.{{ env_name }}_{{ stack_name }}_autoscaling_group.name}"
  }

  alarm_description = "Scale up if CPUReservation is above N% for N duration"
  alarm_actions     = ["${aws_autoscaling_policy.{{ env_name }}_{{ stack_name }}_instance_scale_up.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "{{ env_name }}_{{ stack_name }}_instance_low_cpu" {
  alarm_name          = "alarm_{{ env_name }}_{{ stack_name }}EC2CPUUtilizationLow"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "{{ low_cpu_evaluation_periods |default(2) }}"
  metric_name         = "CPUReservation"
  namespace           = "AWS/EC2"
  period              = "{{ low_cpu_period_seconds |default(300) }}"
  statistic           = "Maximum"
  threshold           = "{{ low_cpu_threshold_percent |default(2)}}"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.{{ env_name }}_{{ stack_name }}_autoscaling_group.name}"
  }

  alarm_description = "Scale down if the CPUReservation is below N% for N duration"
  alarm_actions     = ["${aws_autoscaling_policy.{{ env_name }}_{{ stack_name }}_instance_scale_down.arn}"]

  depends_on = ["aws_cloudwatch_metric_alarm.{{ env_name }}_{{ stack_name }}_instance_high_cpu"]
}

resource "aws_cloudwatch_metric_alarm" "{{ env_name }}_{{ stack_name }}_instance_high_memory" {
  alarm_name          = "alarm_{{ env_name }}_{{ stack_name }}_ClusterMemoryReservationHigh"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "{{ high_memory_evaluation_periods | default(2) }}"
  metric_name         = "MemoryReservation"
  namespace           = "AWS/EC2"
  period              = "{{ high_memory_period_seconds |default(300)}}"
  statistic           = "Maximum"
  threshold           = "{{ high_memory_threshold_percent|default(90)}}"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.{{ env_name }}_{{ stack_name }}_autoscaling_group.name}"
  }

  alarm_description = "Scale up if the MemoryReservation is above N% for N duration"
  alarm_actions     = ["${aws_autoscaling_policy.{{ env_name }}_{{ stack_name }}_instance_scale_up.arn}"]

  depends_on = ["aws_cloudwatch_metric_alarm.{{ env_name }}_{{ stack_name }}_instance_low_cpu"]
}

resource "aws_cloudwatch_metric_alarm" "{{ env_name }}_{{ stack_name }}_instance_low_memory" {
  alarm_name          = "alarm_{{ env_name }}_{{ stack_name }}_ClusterMemoryReservationLow"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "{{ low_memory_evaluation_periods | default(2) }}"
  metric_name         = "MemoryReservation"
  namespace           = "AWS/EC2"
  period              = "{{ low_memory_period_seconds | default(300)}}"
  statistic           = "Maximum"
  threshold           = "{{ low_memory_threshold_percent |default(10) }}"

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.{{ env_name }}_{{ stack_name }}_autoscaling_group.name}"
  }

  alarm_description = "Scale down if the MemoryReservation is below N% for N duration"
  alarm_actions     = ["${aws_autoscaling_policy.{{ env_name }}_{{ stack_name }}_instance_scale_down.arn}"]

  depends_on = ["aws_cloudwatch_metric_alarm.{{ env_name }}_{{ stack_name }}_instance_high_memory"]
}





###########################################################################################
# DNS
###########################################################################################

#This can be done better instead of creating seperate variables for each domain/ip


#data "aws_route53_zone" "knablocal" {
#  name         = "{{ knablocal_route53_zone }}."
#  private_zone = true
#}

resource "aws_route53_zone" "knablocal" {
  name = "{{ knablocal_route53_zone }}"
  #private_zone = true
 }

#resource "aws_route53_record" "{{artifactory_route53_record_name }}" {
resource "aws_route53_record" "knablocal" {
  zone_id = "${aws_route53_zone.knablocal.zone_id}"
  #zone_id = "${data.aws_route53_zone.knablocal.zone_id}"
  #name    = "{{artifactory_route53_record_name }}.{{ knablocal_route53_zone }}"
  name    = "{{  artifactory_route53_record_name  }}.{{ knablocal_route53_zone }}"
  type    = "A"
  #type    = "CNAME"
  ttl     = "300"
  #records = ["{{ artifactory_route53_record_ip }}"]
  records = ["${aws_route53_zone.knablocal.name_servers.0}"]
}